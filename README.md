LivWell Cortez, located in the historic archaeological town of Cortez, officially opened its doors as a recreational marijuana dispensary in early 2015 and has been providing the community with top-notch cannabis products and services ever since. 

Address: 1819 E Main St, Cortez, CO 81321, USA

Phone: 970-565-9577

Website: https://www.livwell.com/cortez-marijuana-dispensary
